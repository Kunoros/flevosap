# Flevosap

## Samenvatting:
Dit project is onderdeel van de module Project Software Development van Windesheim in Almere. Het is de bedoeling om een website te bouwen voor Flevosap, aan de hand van HTML en PHP, om dit te beheersen.

In de root van ons project vinden we nu een aantal bestanden. de <kbd>index.php</kbd>, de <kbd>.gitignore</kbd> en de <kbd>Readme.md</kbd>.

## How to run dev
1: Laadt SQL in de database<br>
2: Composer dump-autoload<br>
3: Start localhost met de command (php -S localhost:8080)<br>

$dbName = 'Flevosap_b3';<br>
$hostName = 'localhost';<br>
$userName = 'root';<br>
$password = 'root';

## How to deploy
1: Login SSH in B3<br>
2: Run het command (cd var/www/html/flevosap)<br>
3: Run het command (git pull)<br>

$dbName = 'Flevosap_b3';<br>
$hostName = 'b3.clow.nl';<br>
$userName = 'flevosap_user';<br>
$password = 'sKy35yOpnmDnVVBZY6Liahtb0rxdo0h9';

## Gebruikers acounts
Dummy accounts die worden aangemaakt bij het initializeren van de database:

Admins:

    E-mail:     admin@email.com
    Password:   admin

Users:

    E-mail:     user@email.com
    Password:   user

