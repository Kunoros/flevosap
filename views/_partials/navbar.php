<link rel="stylesheet" href="assets/css/navbar.css">

<nav class="navbar navbar-expand-lg navbar-light navBackground">
    <div class="container-fluid">
        <a class="navbar-brand" href="."><img class="logo-resize" alt="apple-img" src="assets//img/flevosap-logo.png"></a>
        <button class="navbar-toggler search-btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse flex-fix" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="boomgaard">Boomgaard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="webshop">Sappen</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link dark-nav-link" href="horeca">Horeca</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nieuws">Nieuws</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact">Contact</a>
                </li>
                <?php if(isset($_SESSION['user'])) : if($_SESSION['user']->getRole() == 'ADMIN') : ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="adminDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Administratie</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="admin-product">Producten</a></li>
                        <li><a class="dropdown-item" href="orderlist">Bestellingen</a></li>
                    </ul>
                </li>
                <?php endif; endif;?>
            </ul>
            <form action="search" method="POST" class="d-flex w-50 extra-form-pd">
                <input class="form-control rounded rounded-pill me-2" type="search" placeholder="Search" aria-label="Search" name="search">
            </form>
            <?php if (isset($_SESSION['user'])): ?>
            <button type="button" onclick="window.location.href='winkelwagen';" class="btn btn-light rounded-pill text-dark cart"><i class="bi bi-cart4"></i>
            </button>
            <?php endif ?>
            <div class="log-in-out-mb">
                <?php if (isset($_SESSION['user'])) { ?>
                    <a class="login-link" href="profile">Profile</a>
                <span class="white-line">|</span>
                <a class="login-link" href="logout">Logout</a>
            <?php } else { ?>
                <a class="login-link" href="login"> Inloggen</a>
                <span class="white-line">|</span>
                <a class="login-link" href="register"> Registreren</a>
            <?php } ?>
            </div>
        </div>
    </div>
</nav>