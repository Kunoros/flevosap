<link rel="stylesheet" href="assets/css/footer.css">

<!-- Footer -->
<footer class="text-white text-center text-lg-start">
  <!-- Grid container -->
  <div class="container p-4">
    <!--Grid row-->
    <div class="row">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase">Producten</h5>
                <ul class="list-unstyled mb-0">
                    <li>
                        <a class="nav-link" href="webshop">Sappen</a>
                    </li>
                    <li>
                        <a class="nav-link" href="horeca">Horeca</a>
                    </li>
                </ul>
            </div>
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase">Over ons</h5>
                <ul class="list-unstyled mb-0">
                    <li>
                        <a class="nav-link" href="boomgaard">Boomgaard</a>
                    </li>
                    <li>
                        <a class="nav-link" href="nieuws">Nieuws</a>
                    </li>
                </ul>
        </div>
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase">Contact</h5>
            <ul class="list-unstyled mb-0">
                <li>
                    <a class="nav-link" href="contact">Contactformulier</a>
                </li>
                <li>
                    <br>
                    <h5 class="text-uppercase">Volg ons op</h5>
                    <a href="https://www.facebook.com/Flevosap-121735877999334/"><i class="bi bi-facebook"></i></a>
                </li>
            </ul>
        </div>
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase">Adres</h5>
            <div class="avia_textblock  av_inherit_color " style="color:#ffffff; " itemprop="text"><p><strong>Flevosap bv</strong><br>
                    <strong> Prof. Zuurlaan 22</strong><br>
                    <strong> 8256 PE Biddinghuizen, Nederland</strong><br>
                    <strong> Tel: <strong><strong>+31 (<strong>0)321 – 33 25 25</strong></strong></strong></strong><br>
                    <span style="color: #000000;"><strong> <a style="color: #000000; text-decoration: underline;" href="mailto:info@flevosap.nl">info@flevosap.nl</a></strong></span></p>
            </div>
        </div>
    </div>
        <!--Grid column-->
    <div class="row"
    <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
        <a class="navbar-brand" href=""><img class="logo-resize" alt="apple-img" src="assets/img/flevosap-logo.png"></a>
    </div>
    </div>
    <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    <span>© 2021 Copyright:</span>
    <a class="footer-black-a" href=".">Flevosap.nl</a>
  </div>
    <!-- Copyright -->
</footer>

    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
