<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/boomgaard.css">

    <title>Home - Flevosap</title>
</head>

<body>
<div class="container-fluid">
    <div class="row extra-padding">
        <div class="col-md-2"></div>
        <div class="col-md-8 header-left">
            <h1>Ons sterrenteam!</h1>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-sm-5 margin-text-two align-text-center">
            <h3>Fruit is het mooiste cadeautje van de natuur</h3>
            <p>Familie Vermeulen maakt samen met een team van enthousiaste medewerkers Flevosap. Ze zijn
                bijna iedere dag te vinden in de boomgaarden rondom hun boerderij en bij collega-kwekers die ook hun
                bijdrage leveren aan
                Flevosap. Het fruit, dat is hun passie. Ze zijn er met hart en ziel aan verknocht. Het maakt niet uit of
                je hen iets vraagt over appels, sinaasappels, aardbeien, citroenen, kersen, zwarte bessen, peren of
                cranberry’s: zij weten welk fruit in Flevosap mag, en welke niet. Bij de appels zijn bijvoorbeeld de
                Elstar, Goudrenet en Jonagold favoriet</p>
        </div>
        <div class="col-sm-5">
            <img src="assets/img/VruchtenB.png" class="image-resize-one img-fluid">
        </div>
        <div class="col-md-1"></div>

        <div class="col-md-1"></div>
        <div class="col-sm-5">
            <img src="assets/img/boomgaard.png"
                 class="image-resize-two img-fluid">
        </div>
        <div class="col-sm-5 margin-text-two align-text-center">
            <h3>Over smaak valt niet te twisten…</h3>
            <p>…zegt het spreekwoord. Nou, daar zijn ze het bij Flevosap dus mooi niet altijd mee eens. Want als
                het op proeven aankomt, heeft iedereen een eigen idee over de smaak. Maar na dagen proeven, proeven
                en nog eens proeven komen ze er altijd uit. Dan hebben ze precies de echte Flevosapsmaak te pakken.</p>
        </div>
        <div class="col-md-1"></div>

        <div class="col-md-1"></div>
        <div class="col-sm-5 margin-text-two align-text-center">
            <h3>Perfecte match</h3>
            <p>Wat dat is? Puur vakmanschap van jarenlange ervaring in het telen van fruit op de
                tikkeltje zilte Flevolandse bodem, boordevol zeemineralen. Dat geeft natuurlijk het beste en
                sappigste fruit van de gulste bomen. Bij Flevosap weten ze precies welke fruitsoorten wel en welke
                niet matchen.</p>
        </div>
        <div class="col-sm-5">
            <img src="assets/img/cassis.png" class="image-resize-one img-fluid">
        </div>
        <div class="col-md-1"></div>
    </div>
</div>

</body>

</html>