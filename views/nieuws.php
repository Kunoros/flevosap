<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/nieuws.css">

    <title>Nieuws - Flevosap</title>
</head>
<body>
<div class="container-fluid">
    <div class="row nieuws-header">
        <div class="col">
            <h1 class="nieuws-header-text">De sappigste nieuwtjes</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 zoom p-0">
            <div class="card nieuws-margin">
                <img src="assets/img/fruitteeltbedrijf-rozendaal.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Fruitteeltbedrijf Rozendaal</h5>
                    <p class="card-text">'Alles voor de knapperigste, sappigste Elstars.'</p>
                    <p class="card-subtitle mb-2 text-muted">1 oktober 2021</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 zoom p-0">
            <div class="card nieuws-margin">
                <img src="assets/img/gastvrij-rotterdam.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Kom puur proeven op Gastvrij Rotterdam 2021!</h5>
                    <p class="card-text">Wat hebben we hier naar uitgekeken; elkaar weer écht ontmoeten.</p>
                    <p class="card-subtitle mb-2 text-muted">7 september 2021</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 zoom p-0">
            <div class="card nieuws-margin">
                <img src="assets/img/fruitteler-sander.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Fruitteler Sander van de Rijdt</h5>
                    <p class="card-text">'Ik ervaar het hele jaar geluksmomentjes.'</p>
                    <p class="card-subtitle mb-2 text-muted">6 september 2021</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 zoom p-0">
            <div class="card nieuws-margin">
                <img src="assets/img/ijs-flevosap.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">IJs Flevosap</h5>
                    <p class="card-text">Wij van Flevosap zijn een samenwerking aangegaan met "De IJskoffer".</p>
                    <p class="card-subtitle mb-2 text-muted">4 mei 2021</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 zoom p-0">
            <div class="card nieuws-margin">
                <img src="assets/img/fssc-certificaat.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">FSSC-220000 certificaat</h5>
                    <p class="card-text">Hoera! Het nieuwe FSSC-22000 certificaat is binnen!</p>
                    <p class="card-subtitle mb-2 text-muted">4 mei 2021</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 zoom p-0">
            <div class="card nieuws-margin">
                <img src="assets/img/fssc-22000.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">FSSC-22000 certificaat</h5>
                    <p class="card-text">Wij gaan voor ons voedselveiligheidsprogramma naar FSSC 22000.</p>
                    <p class="card-subtitle mb-2 text-muted">1 februari 2021</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>