<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>


<body>
<div class="container-fluid col-8">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <form action="order" method="post">
                    <img src="/assets/img/ideal-logo.jpg" alt="ideal logo" width="100" height="100">
                    <select class="form-select" aria-label="">
                        <option selected>Kies een bank</option>
                        <option value="1">ABN-Amro Bank</option>
                        <option value="2">ASN Bank</option>
                        <option value="3">Bunq</option>
                        <option value="4">ING Bank</option>
                        <option value="5">Knab</option>
                        <option value="6">Rabobank</option>
                        <option value="7">SNS Bank</option>
                        <option value="8">Triodos Bank</option>
                    </select>
                        <button type="submit" class="btn btn-primary">Verder</button>
                        <input type="hidden" name="pay" value="succes">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>