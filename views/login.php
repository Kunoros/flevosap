<?php
include_once '_partials/header.php';
include_once '_partials/navbar.php';
?>
<br>
<!doctype html>
<html lang="en">
<head>

    <link rel="stylesheet" href="assets/css/login.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Log in</title>

</head>

<body>

<div class="container mt-5 mb-5">

    <div class="card bg-blue">
        <article class="card-body mx-auto" style="max-width: 400px;">
            <h4 class="card-title mt-3 text-center">Account Inloggen</h4>
            <p class="text-center">Vul je gegevens in.</p>
            <form class="mt-4" action="login-post" method="post">
                <div class="form-group input-group">
                    <input name="email" class="form-control" placeholder="Email" type="text" required>
                </div>
                <div class="form-group input-group">
                    <input name="password" class="form-control" placeholder="Wachtwoord" type="password" required>
                </div>
                <div class="form-group">
                    <button name="submit" type="submit" class="btn btn-primary btn-block"> Inloggen</button>
                </div>
                <?php
        if (!isset($_GET['login'])) {
            // exit();
        } else {
            $loginCheck = $_GET['login'];

            if ($loginCheck == "email not found") {
                echo "<p class='mt-4 text-center login-error'>Email does not exist!</p>";
                //! if exit() is used _partials/footer.php not showing
                // exit();
            } elseif ($loginCheck == "wrong password") {
                echo "<p class='mt-4 text-center login-error'>Wrong password!</p>";
            } 
            
        }
        ?>
            </form>
        </article>
    </div>

</div>
<br>


</body>