<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/profile.css">
    <title>Profile</title>
</head>

<body>
    <div class="container main-container">
        <div class="row">
            <div class="col-md-4 mt-1">
                <div class="card text-center sidebar">
                    <div class="card-body">
                        <div class="mt-3">
                            <h2><?= $_SESSION['user']->getFirstName() . ' ' . $_SESSION['user']->getLastName(); ?></h2>
                            <hr>
                            <button class="btn btn-primary m-2" type="button" data-bs-toggle="collapse" data-bs-target="#edit" aria-expanded="false" aria-controls="edit">
                                Gegevens aanpassen
                            </button>
                            <?php
                            if (!isset($_GET['edit'])) {
                            } else {
                                $profileCheck = $_GET['edit'];

                                if ($profileCheck == "Something went wrong try again") {
                                    echo "<p class='mt-4 text-center login-error'>Er is iets fout gegaan probeer het opnieuw!</p>";
                                } elseif ($profileCheck == "User not found") {
                                    echo "<p class='mt-4 text-center login-error'>Gebruiker niet gevonden!</p>";
                                } elseif ($profileCheck == "Verify went wrong") {
                                    echo "<p class='mt-4 text-center login-error'>Het oude wachtwoord is fout!</p>";
                                }
                            }
                            ?>
                            <br>
                            <button class="btn btn-primary m-2" type="button" data-bs-toggle="collapse" data-bs-target="#history" aria-expanded="false" aria-controls="history">
                                Bestellingen
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 mt-1">

                <div class="card md-3 content collapse" id="edit">
                    <h1 class="m-3 pt-3">Gegevens aanpassen</h1>
                    <div class="card-body">
                        <form method="post" action="update-profile">
                            <input type="hidden" value="<?= $_SESSION['user']->getId(); ?>" name="id">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="firstName">First name</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="firstName" type="text" name="firstName" class="form-control" value="<?= $_SESSION['user']->getFirstName(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="lastName">Last name</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="lastName" type="text" name="lastName" class="form-control" value="<?= $_SESSION['user']->getLastName(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="email" type="email" name="email" class="form-control" value="<?= $_SESSION['user']->getEmail(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="street">Straat</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="street" type="street" name="street" class="form-control" value="<?= $_SESSION['user']->getStreet(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="street">huisnummer</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="houseNumber" type="number" name="houseNumber" class="form-control" value="<?= $_SESSION['user']->getHouseNumber(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="street">Postcode</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="postalCode" type="text" name="postalCode" class="form-control" value="<?= $_SESSION['user']->getPostalCode(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="street">Stad</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="city" type="city" name="city" class="form-control" value="<?= $_SESSION['user']->getCity(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <label for="newPassword">New password</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="newPassword" type="password" name="new_password" class="form-control">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <label for="confirmPassword">Confirm password</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="confirmPassword" type="password" name="new_password2" class="form-control">
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <label for="oldPassword">Old password</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="oldPassword" type="password" name="old_password" class="form-control">
                                </div>
                            </div>
                            <hr>
                            <div class="row mt-4 justify-content-center">
                                <div class="col-md-3 justify-content-center">
                                    <div class="mb-3 mt-2">
                                        <button class="btn ml-4" type="submit" name="submit">Change!</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card md-3 content">
                    <h1 class="m-3 pt-3">Gegevens</h1>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Volledige naam</h5>
                            </div>
                            <div class="col-md-9 text-secondary">
                                <p><?= $_SESSION['user']->getFirstName() . ' ' . $_SESSION['user']->getLastName() ?></p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Email</h5>
                            </div>
                            <div class="col-md-9 text-secondary">
                                <p><?= $_SESSION['user']->getEmail() ?></p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Adres</h5>
                            </div>
                            <div class="col-md-9 text-secondary">
                                <p><?= $_SESSION['user']->getStreet() . ' ' . $_SESSION['user']->getHouseNumber() ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9 text-secondary">
                                <p><?= $_SESSION['user']->getPostalCode() . ' ' . $_SESSION['user']->getCity() ?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card md-3 content collapse" id="history">
                    <h1 class="m-3 pt-3">Bestellingen</h1>
                    <div class="table-responsive-md">
                        <table class="table table-light table-striped extra-marginbot table-content">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Totale prijs</th>
                                <th scope="col">Besteldatum</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($orderHistories as $orderHistory) : ?>
                            <tr>
                                <td><?= $orderHistory['cartId'] ?></td>
                                <td>€<?= $orderHistory['finalPrice'] ?></td>
                                <td><?= date("d/m/Y", strtotime($orderHistory['createdAt'])) ?></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>