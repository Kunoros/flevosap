<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <link rel="stylesheet" href="../assets/css/contact.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Required bootstrap from _partials -->


    <title>Contact</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-sm">
            <img src="assets/img/mail.png" class="rounded mx-auto d-block float-middle contact-icon" alt="">
            <div class="card float-middle rounded-pill">
                <div class="card-body col text-center" >
                    <h5 class="card-title">Flevosap bv</h5>
                    <h5 class="card-title">Prof. Zuurlaan 22</h5>
                    <h5 class="card-title">8256 PE Biddinghuizen, Nederland</h5>
                    <h5 class="card-title">Tel: +31 (0)321 - 33 25 25</h5>
                    <h5 class="card-title">info@flevosap.nl</h5>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="container py-4">

                <!-- Bootstrap 5 starter form -->
                <form class="contactForm" action="/contact" method="post">

                    <!-- Name input -->
                    <div class="mb-3 ">
                        <label class="form-label" for="name">Naam</label>
                        <input class="form-control rounded-pill" type="text" name="name" placeholder="Naam" required>
                    </div>

                    <div class="mb-3">
                        <label class="form-label" for="achternaam">Achternaam</label>
                        <input class="form-control rounded-pill" type="text" name="achternaam" placeholder="Achternaam" required>
                    </div>
                    <!-- Email address input -->
                    <div class="mb-3">
                        <label class="form-label" for="emailAddress">Email Address</label>
                        <input class="form-control rounded-pill" type="email" name="mail" placeholder="E-mail" required>
                    </div>

                    <!-- Message input -->
                    <div class="mb-3">
                        <label class="form-label" for="message">Uw vraag</label>
                        <textarea class="form-control rounded-lg" name="onderwerp" type="text" placeholder="Uw vraag" style="height: 10rem;" required></textarea>
                    </div>

                    <!-- Form submit button -->
                    <div class="d-grid">
                        <button class="btn btn-primary btn-lg rounded-pill" type="submit" name="submit">Submit</button>
                    </div>
                    <div class="row">
                        <?php
                        if (isset($_GET['contact'])) {
                            $msgConfirm = $_GET['contact'];
                            echo "<p class='mt-4 msg-confirm'>Uw vraag is verzonden!</p>";
                        }
                        ?>
                    </div>

                </form>

            </div>
        </div>

    </div>
</div>

</body>

</html>