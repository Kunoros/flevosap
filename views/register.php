<!doctype html>
<html lang="en">
<head>

    <link rel="stylesheet" href="assets/css/register.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registratie</title>

</head>

<body>

<div class="container">

    <div class="">
        <article class="card-body mx-auto" style="max-width: 400px;">
            <h4 class="card-title mt-3 text-center">Account creëren</h4>
            <p class="text-center">Vul je gegevens in.</p>
            <form action="register-post" method="post">
                <div class="form-group input-group">
                    <input require name="firstName" class="form-control col-md-6" placeholder="Voornaam" type="text" required>
                    <input require name="lastName" class="form-control col-md-6" placeholder="Achternaam" type="text" required>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <input require name="email" class="form-control" placeholder="E-mailadres" type="email" required>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <input require name="password" class="form-control" placeholder="Wachtwoord" type="password" required>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <input require name="street" class="form-control" placeholder="Straat" type="text" required>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <input require name="houseNumber" class="form-control col-md-6" type="number" placeholder="Huisnummer" type="text"
                           required>
                    <input require name="postcode" class="form-control col-md-6" placeholder="Postcode" type="text" required>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <input require name="city" class="form-control" placeholder="Stad" type="text" required>
                </div> <!-- form-group// -->
                <div class="form-group">
                    <button require name="submit" type="submit" class="btn btn-primary btn-block"> Account aanmaken</button>
                </div> <!-- form-group// -->
                <p class="text-center">Heb je al een account? <a href="login">Inloggen</a></p>
            </form>
            <?php
        if (!isset($_GET['register'])) {
            // exit();
        } else {
            $registerCheck = $_GET['register'];

            if ($registerCheck == "email already in use") {
                echo "<p class='mt-4 text-center login-error'>The email is already in use!</p>";
                //! if exit() is used _partials/footer.php not showing
                // exit();
            }
        }
        ?>
        </article>
    </div>

</div>

</body>