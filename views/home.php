<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/home.css">

    <title>Home - Flevosap</title>
</head>
<?php
$alert = false;
if(empty($_SESSION['cart'])):
$_SESSION['cart'] = array();
endif;
?>
<body>
    <div class="container-fluid">
        <div class="row extra-padding">
            <div class="col-md-3"></div>
            <div class="col-md-6 header-center">
                <h1>Liefhebbers van Flevosap: welkom!</h1>
                <p class="center-block text-center margin-text font-home">De vele variaties zorgen voor de ene fruitige verrassing
                    na de andere. De pure smaak van één soort, of een spannende combinatie van twee of meer. Altijd 100
                    procent natuurlijk, dus zonder smaak- en conserveringsmiddelen. Waar je ook bent, met Flevosap heb je altijd de
                    smaak te pakken!</p>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-sm-7">
                <img src="assets/img/flevosap-banner.jpg" class="image-resize-one img-fluid rounded rounded-pill">
            </div>
            <div class="col-sm-4 margin-text-two align-text-center">
                <h3>Het sap waar je de smaak ziet zitten</h3>
                <p>De grote flessen kom je tegen in je supermarkt. Dat is makkelijk meenemen, en kinderen zijn er dol op.
                    Volwassenen trouwens ook. In de winkel vind je ook een handig klein flesje dat graag mee onderweg gaat.
                    Zoals naar (sport)school of werk, een dagje uit, of in je rugzak op vakantie.</p>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1"></div>
            <div class="col-sm-4 margin-text-two align-text-center">
                <h3>Altijd en overal genieten</h3>
                <p>Gezellig samen wat eten en drinken in een café, lunchroom of restaurant? Ook daar kun je genieten van de
                    smaak van vers fruit. Om zeker te weten dat je de echte krijgt, bestel je niet zo maar een sapje, maar
                    vraag je om Flevosap!</p>
            </div>
            <div class="col-sm-7">
                <img src="assets/img/boomgaard-biologische-appels.jpg" class="image-resize-two img-fluid rounded rounded-pill">
            </div>
        </div>
    </div>
    <script>
        function BestellingVoltooid()
        {
            alert("Uw bestelling is geplaatst");
        }
    </script>
    <?php if($_SESSION['alert'] == true):
    echo '<script>BestellingVoltooid()</script>';
    $_SESSION['cart'] = array();
    $_SESSION['alert'] =  false;
    endif; ?>
</body>

</html>