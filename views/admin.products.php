<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/table.css">
    <title>Admin</title>
</head>
<style>
</style>
<body>
<div class="container-fluid wrapper">
    <div class="row">
        <div class="col-1">
        </div>
        <div class="col">
            <div class="card card-grid" role="grid" aria-labelledby="gridLabel">
                <div class="card-header">
                    <div class="row" role="row">
                        <div class="col-md-2" role="columnheader">
                            <p class="form-control-plaintext">Naam</p>
                        </div>
                        <div class="col-md" role="columnheader">
                            <p class="form-control-plaintext">Voorraad</p>
                        </div>
                        <div class="col-md" role="columnheader">
                            <p class="form-control-plaintext">Prijs</p>
                        </div>
                        <div class="col-md-4" role="columnheader">
                            <p class="form-control-plaintext">Beschrijving</p>
                        </div>
                        <div class="col-md-2" role="columnheader">
                            <p class="form-control-plaintext">Voedingswaarde</p>
                        </div>
                        <div class="col-md-1" role="columnheader">
                            <p class="form-control-plaintext text-right"><i onclick="addProduct()" style="font-size: 1.5em" class="bi bi-plus-lg clickable"></i></p>
                        </div>
                        <!--more column headers here-->
                    </div>
                    <div id="gridLabel" class="card-grid-caption">
                        <p class="form-control-plaintext">Producten</p>
                    </div>
                </div>
                <div class="card-body" id="product_row_body">
                    <?php foreach ($products as $product) :
                    $nutrition = $product->getNutrition();
                        ?>
                        <div productid="<?= $product->getId() ?>" class="row productInput" role="row"
                             id="product_id_<?= $product->getId() ?>">
                            <div class="col-md-2" role="gridcell">
                                <label>Naam</label>
                                <input name="name" type="text" class="form-control" value="<?= $product->getName(); ?>">
                            </div>
                            <div class="col-md" role="gridcell">
                                <label>Voorraad</label>
                                <input name="stock" type="number" class="form-control"
                                       value="<?= $product->getStock(); ?>">
                            </div>
                            <div class="col-md" role="gridcell">
                                <label>Prijs</label>
                                <input name="price" type="number" class="form-control"
                                       value="<?= $product->getPrice(); ?>">
                            </div>
                            <div class="col-md-4" role="gridcell">
                                <label>Beschrijving</label>
                                <textarea name="description" style="min-height: 150px;" type="text"
                                          class="form-control"><?= $product->getDescription(); ?></textarea>
                            </div>
                            <div class="col-md-2" role="gridcell">
                                <label>Voedingswaarde</label>
                                <table class="nutrition">
                                    <tr>
                                        <th>Kj</th>
                                        <td> <input type="number" class="nutrition-form-ctrl" name="energyKj" value="<?=$nutrition->getEnergyKj();?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Kcal</th>
                                        <td><input type="number" class="nutrition-form-ctrl" name="energyKcal" value="<?= $nutrition->getEnergyKcal(); ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Vetten</th>
                                        <td><input name="fats" type="number" class="nutrition-form-ctrl" value="<?= $nutrition->getFats(); ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Waarvan verzadigd</th>
                                        <td><input name="saturatedfats" type="number" class="nutrition-form-ctrl" value="<?= $nutrition->getSaturatedFats(); ?>"</td>
                                    </tr>
                                    <tr>
                                        <th>Koolhydraten</th>
                                        <td><input type="number" class="nutrition-form-ctrl" name="carbohydrates" value="<?= $nutrition->getCarbohydrates(); ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Waarvan suiker</th>
                                        <td><input type="number" class="nutrition-form-ctrl" name="sugars" value="<?= $nutrition->getSugars(); ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Eiwitten</th>
                                        <td><input type="number" class="nutrition-form-ctrl" name="protein" value="<?= $nutrition->getProtein(); ?>"></td>
                                    </tr>
                                    <tr>
                                        <th>Zout</th>
                                        <td><input type="number" class="nutrition-form-ctrl" name="salt" value="<?= $nutrition->getSalt(); ?>"></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md text-center" role="gridcell">
                                <div class="actionWrapper">
                                    <i onclick="removeProduct(<?= $product->getId() ?>)"
                                       class="bi bi-trash-fill ho remove clickable"></i>
                                    <i onclick="updateProduct(<?= $product->getId() ?>)"
                                       class="bi bi-save-fill ho save clickable"></i>

                                </div>
                                <div class="product-images">
                                    <img src="product/image?id=<?= $product->getId() ?>" class='image' alt='img'>
                                    <div class="imageUpload">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-x-lg clickable svg cancel" viewBox="0 0 16 16" onclick="cancelImage(<?=$product->getId()?>)">
                                            <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
                                            <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
                                        </svg>
                                        <label class="uploadLabel" for="file_upload_<?= $product->getId() ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-upload clickable svg" viewBox="0 0 16 16">
                                                <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                                <path d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z"/>
                                            </svg>
                                        </label>
                                        <input name="IMAGE" type="file" accept=".jpg, .jpeg, .png" class="imageInput"
                                               id="file_upload_<?= $product->getId() ?>" hidden>
                                    </div>
                                </div>
                            </div>
                            <!--more cells here-->
                        </div>
                    <?php endforeach; ?>
                    <!--more rows here-->
                </div>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
<script src="public/js/productEdit.js"></script>
<!-- Required footer from _partials -->
</body>
</html>