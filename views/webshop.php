<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/product.css">
    <!-- Required bootstrap from _partials -->

    <title>Sappen - Flevosap</title>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col"></div>
        <div class="game-container col-10">

            <?php foreach ($products as $product) : ?>
                <div class="card" style='width: 18rem;'>
                    <img src='assets/img/<?= $product->getId(); ?>.jpg' class='card-img-top' alt='img'>
                    <div class="card-body text-center">
                        <h5 class="card-title"><?= $product->getName(); ?></h5>
                        <h4 class="card-text text-danger">Prijs: €<?= number_format($product->getPrice(),2) ?></h4>
                        <div class="mt-3">
                            <button type="button"
                                    onclick="window.location.href='products?page=product&id=<?= $product->getId() ?>'"
                                    class="btn btn-outline-primary btn-block">Bekijk item
                            </button>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
        <div class="col"></div>
        <div>
        </div>
    </div>
</div>

</body>

</html>