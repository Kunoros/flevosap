<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/product.css">

    <title>Producten</title>
</head>
<body>

<div class="container-fluid" style="margin-top: 70px; margin-bottom: 70px;">
    <div class="row d-flex justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="row">
                    <div class="col-md-6">
                        <div class="images p-3">
                            <div class="text-center p-4">
                                <img id="main-image" src='assets/img/<?= $product->getId()?>.jpg' class='card-img-top image' alt='img'>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 test">
                        <div class="product p-4">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center"><i class="fa fa-long-arrow-left"></i> <a
                                            href="webshop"><span class="ml-1" href="test">Terug</span></a></div>
                            </div>
                            <div class="mt-4 mb-3">
                                <h5 class="text-uppercase"><?= $product->getName(); ?></h5>
                                <div class="price d-flex flex-row align-items-center">
                                    <div class="ml-2"><p>Prijs: € <?= number_format($product->getPrice(),2); ?></p></div>
                                </div>
                            </div>
                            <p class="about"><?= $product->getDescription(); ?></p>
                            <div class="container" style="margin-top: 20px">
                                <div class="row">
                                    <div class="col">
                                        <ul class="b">
                                            <li>Energy: <?= $product->getNutrition()->getEnergyKj(); ?> Kj
                                                (<?= $product->getNutrition()->getEnergyKcal(); ?> Kcal)
                                            </li>
                                            <li>vetten: <?= $product->getNutrition()->getFats(); ?> g</li>
                                            <li>Waarvan verzadigd: <?= $product->getNutrition()->getSaturatedFats() ?> g</li>
                                            <li>Koolhydraten: <?= $product->getNutrition()->getCarbohydrates(); ?> g</li>
                                        </ul>
                                    </div>
                                    <div class="col">
                                        <li>Waarvan suiker:<?= $product->getNutrition()->getSugars(); ?> g</li>
                                        <li>Eiwitten: <?= $product->getNutrition()->getProtein();?> g</li>
                                        <li>Zout: <?= $product->getNutrition()->getSalt(); ?> g</li>
                                    </div>
                                </div>
                            </div>
                            <div class="cart mt-4 align-items-center">
                                <?php if ($product->getStock() == 0): ?>
                                <button class="btn btn-danger text-uppercase mr-2 px-4 primary disabled" aria-disabled="true"">tijdelijk niet op voorraad
                                </button>
                                    <?php else: ?>
                                 <form action="winkelwagen" method="POST">
                                    <div class="cart mt-4 align-items-center">
                                    <input type="hidden" name="productid" value="<?=$product->getId()?>">
                                    <input type="hidden" name="quantity" value="1">
                                    <input type="submit" name="submit" class="btn btn-danger text-uppercase mr-2 px-4" value="Aan winkelwagen toevoegen">
                                </div>
                            </form>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
