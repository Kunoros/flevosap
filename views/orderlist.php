<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/home.css">

    <title>Orderlist</title>
</head>

<body>
<div class="container-fluid">
    <div class="col-md-12">
        <div class="container col-md-8 bg-white mt-4 mb-4">
            <div class="row border">
                <div class="col-md-12"><strong>Bestellingen</strong></div>
            </div>
            <?php foreach ($orders as $order) : ?>
                <div class="row">
                    <div class="col-md-4 border">
                        <div class="row">
                            <div class="col-md-12">
                                <strong>Klant gegevens :</strong> <br>
                                <?= $order->getUser()->getfirstname() ?>&nbsp;<?= $order->getUser()->getlastname() ?> <br>
                                <?= $order->getUser()->getemail() ?> <br>
                                <?= $order->getUser()->getstreet() ?>&nbsp;<?= $order->getUser()->gethouseNumber() ?> <br>
                                <?= $order->getUser()->getstreet() ?> <br>
                                <?= $order->getUser()->getpostalCode() ?> <br>
                                <?= $order->getUser()->getcity() ?> <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><strong>Verzendcode :</strong></div>
                        </div>
                    </div>
                    <div class="col-md-8 border">
                        <div class="align-text-left">
                            <strong>OrderID :</strong> <?= $order->formatId() ?>
                            <table>
                            <?php $total=0;?>
                                <?php foreach($order->getProducts() as $cartItem) :
                                    $product = $cartItem['product']?>
                            <tr>
                                <td style="width: 50px"><?= $cartItem['amount'] ?> X</td>
                                <td style="width: 200px"><?= $product-> getName() ?></td>
                                <td>&euro;<?= ($product-> getPrice() * $cartItem['amount']) ?></td>
                                <?php $total+=($product-> getPrice() * $cartItem['amount']);?>
                            </tr>
                                <?php endforeach;?>
                            <tr>
                                <td></td>
                                <td><br><strong>Totaal :</strong></td>
                                <td><br>&euro;<?= $total?></td>
                            </tr>
                            </table>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col"></div>
    </div>

</div>
</body>

</html>