<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/css/table.css">
    <title>Gebruikers</title>
</head>
<style>
</style>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-1">
        </div>
        <div class="col">
            <div class="card card-grid" role="grid" aria-labelledby="gridLabel">
                <div class="card-header">
                    <div class="row" role="row">
                        <div class="col-md" role="columnheader">
                            <p class="form-control-plaintext">Naam</p>
                        </div>
                        <div class="col-md-2" role="columnheader" style="flex-shrink: 2">
                            <p class="form-control-plaintext">Email</p>
                        </div>
                        <div class="col-md-2" role="columnheader">
                            <p class="form-control-plaintext">Adres</p>
                        </div>
                        <!--more column headers here-->
                    </div>
                    <div id="gridLabel" class="card-grid-caption">
                        <p class="form-control-plaintext">Gebruikers</p>
                    </div>
                </div>
                <div class="card-body">
                    <?php foreach ($users as $user) : ?>
                        <div userid="<?= $user->getId()?>" class="row productInput" role="row" id="product_id_<?=$user->getId()?>">
                            <div class="col-md" role="gridcell">
                                <label>Naam</label>
                                <span><?= $user->getFirstName();?> <?= $user->getLastName();?></span>
                            </div>
                            <div class="col-md-2" role="gridcell">
                                <label>Email</label>
                                <span><?= $user->getEmail();?></span>
                            </div>
                            <div class="col-md-2" role="gridcell">
                                <label>Adres</label>
                                <span><?= $user->getAddress()?></span>
                            </div>
                            <!--more cells here-->
                        </div>
                    <?php endforeach; ?>
                    <!--more rows here-->
                </div>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
<!-- Required footer from _partials -->
</body>
</html>