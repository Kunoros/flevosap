<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/horeca.css">

    <title>Horeca - Flevosap</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col horeca-header-center font-home">
            <h1>Flevosap in de horeca</h1>
        </div>
    </div>

    <div class="row">
        <div class="col horeca-header-right">
            <h3>Speciaal om uw gasten en klanten te verwennen</h3>
            <p>Naast de consumentenlijn ook een assortiment voor de horeca. Behalve de 1,0L-fles zijn dat de Flevosap
                mini’s. Een mooi maatje van 0,2L en gebotteld in een klassiek glazen flesje om aan tafel te serveren.
                Meer weten over de mogelijkheden om Flevosap in uw assortiment op te nemen? Bel <a href="tel:+31(0)321-332525">+31 (0)321 – 33 25 25</a> of
                mail naar <a href="mailto:verkoop@flevosap.nl">verkoop@flevosap.nl.</a></p>
        </div>
    </div>

    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/1-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Appel 1 liter</h3>
            <p>De meest gegeten appel van Nederland is de Elstar. Uit de hand vinden wij die ook het lekkerst, maar wij
                hebben gemerkt dat als je er ook Jonagold en nog een paar verrassende appelsoorten bij het sap doet, het
                sap een frisse en nog lekkerdere smaak krijgt. Gelukkig groeien deze appels ook in de boomgaarden in de
                Flevopolder.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 194 kJ (46 kcal)</p>
            <p>Vetten: 0,0 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 11,2 g</p>
            <p>Waarvan suikers: 10,5 g</p>
            <p>Eiwitten: 0,1 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/2-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Appel klassiek flesje 0,2 liter</h3>
            <p>De meest gegeten appel van Nederland is de Elstar. Uit de hand vinden wij die ook het lekkerst, maar wij
                hebben gemerkt dat als je er ook Jonagold en nog een paar verrassende appelsoorten bij het sap doet, het
                sap een frisse en nog lekkerdere smaak krijgt. Gelukkig groeien deze appels ook in de boomgaarden in de
                Flevopolder.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 194 kJ (46 kcal)</p>
            <p>Vetten: 0,0 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 11,2 g</p>
            <p>Waarvan suikers: 10,5 g</p>
            <p>Eiwitten: 0,1 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/3-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Peer klassiek flesje 0,2 liter</h3>
            <p>Een gevoelig typje.
                Er staan ook perenbomen in onze boomgaard in Flevoland.
                Net als onze appels zijn deze peren bedoeld om gewoon te eten.
                Zulke lekkere sappige peren dat het logisch is dat we daar ook lekker sap van maken.
                Onze peren zijn echt gevoelige typetjes, daarom een vleugje citroensap erbij om de snelle bruin-kleuring
                tegen te gaan.
                Perensap is lekker voor elk moment van de dag.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 150 kJ (35 kcal)</p>
            <p>Vetten: 0,0 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 8,7 g</p>
            <p>Waarvan suikers: 8,7 g</p>
            <p>Eiwitten: 0,1 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/4-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Sinaasappel klassiek flesje 0,2 liter</h3>
            <p>Sinaasappelsap is het meest gedronken sap. Ook bij de familie Vermeulen werd wel eens een sinaasappeltje
                geperst voor bij het ontbijt. Van het een kwam het ander. Wat begon met een handgeperst sapje bij het
                ontbijt werd sap voor de buurt, voor de Flevopolder en omdat het zo lekker is, voor heel Nederland.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 188 kJ (45 kcal)</p>
            <p>Vetten: 0,1 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 9,1 g</p>
            <p>Waarvan suikers: 8,9 g</p>
            <p>Eiwitten: 1,9 g</p>
            <p>Zout: 0,3 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/5-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Appel-Ananas-Perzik klassiek flesje 0,2 liter</h3>
            <p>Bij Flevosap houden we wel van een feestje. Vele vruchten zijn al in huis. Wie zullen we nog meer
                uitnodigen? Natuurlijk, ananas en perzik! Samen met appel maken we er een fantastisch feest van.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 194 kJ (46 kcal)</p>
            <p>Vetten: 0,1 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 11,1 g</p>
            <p>Waarvan suikers: 10,4 g</p>
            <p>Eiwitten: 0,3 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/6-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Appel Horeca flesje 0,2 liter</h3>
            <p>De meest gegeten appel van Nederland is de Elstar. Uit de hand vinden wij die ook het lekkerst, maar wij
                hebben gemerkt dat als je er ook Jonagold en nog een paar verrassende appelsoorten bij het sap doet, het
                sap een frisse en nog lekkerdere smaak krijgt. Gelukkig groeien deze appels ook in de boomgaarden in de
                Flevopolder.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 194 kJ (46 kcal)</p>
            <p>Vetten: 0,0 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 11,2 g</p>
            <p>Waarvan suikers: 10,5 g</p>
            <p>Eiwitten: 0,1 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/7-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Appel-Aardbei Horeca flesje 0,2 liter</h3>
            <p>Flevosap Appel Aardbei is een heerlijk zomer sap. Zomerkoninkjes – zoals aardbeien ook wel genoemd worden
                – zijn de snoepjes van de natuur. Het zoete zomerse van de aardbei gecombineerd met het frisse van appel
                heeft een smaak die iedereen wel ziet zitten.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 198 kJ (47 kcal)</p>
            <p>Vetten: 0,0 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 11,4 g</p>
            <p>Waarvan suikers: 10,6 g</p>
            <p>Eiwitten: 0,2 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/8-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Appel-Ananas-Perzik Horeca flesje 0,2 liter</h3>
            <p>Bij Flevosap houden we wel van een feestje. Vele vruchten zijn al in huis. Wie zullen we nog meer
                uitnodigen? Natuurlijk, ananas en perzik! Samen met appel maken we er een fantastisch feest van.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 194 kJ (46 kcal)</p>
            <p>Vetten: 0,1 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 11,1 g</p>
            <p>Waarvan suikers: 10,4 g</p>
            <p>Eiwitten: 0,3 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/9-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Appel-Zwarte Bes Horeca flesje 0,2 liter</h3>
            <p>Wij wilden ook een rood sap. Dat kan met kers en met zwarte bessen. Dus hebben ze beiden geprobeerd. Het
                zag er meteen goed uit. En de smaak was super. Maar welke is nou lekkerder? Daar kwamen de broers niet
                uit. Dit is de Appel Zwarte Bes, het andere rode sap voor in de horeca is Appel-Aardbei.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 200 kJ (47 kcal)</p>
            <p>Vetten: 0,0 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 11,3 g</p>
            <p>Waarvan suikers: 10,7 g</p>
            <p>Eiwitten: 0,1 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/10-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Peer Horeca flesje 0,2 liter</h3>
            <p>Een gevoelig typje. Er staan ook perenbomen in onze boomgaard in Flevoland. Net als onze appels zijn deze
                peren bedoeld om gewoon te eten. Zulke lekkere sappige peren dat het logisch is dat we daar ook lekker
                sap van maken. Onze peren zijn echt gevoelige typetjes, daarom een vleugje citroensap erbij om de snelle
                bruin-kleuring tegen te gaan. Perensap is lekker voor elk moment van de dag.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 150 kJ (35 kcal)</p>
            <p>Vetten: 0,0 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 8,7 g</p>
            <p>Waarvan suikers: 8,7 g</p>
            <p>Eiwitten: 0,1 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left">
        <div class="col-md-4">
            <img src="assets/img/11-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Sinaasappel Horeca flesje 0,2 liter</h3>
            <p>Sinaasappelsap is het meest gedronken sap. Ook bij de familie Vermeulen werd wel eens een sinaasappeltje
                geperst voor bij het ontbijt. Van het een kwam het ander. Wat begon met een handgeperst sapje bij het
                ontbijt werd sap voor de buurt, voor de Flevopolder en omdat het zo lekker is, voor heel Nederland.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 188 kJ (46 kcal)</p>
            <p>Vetten: 0,1 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 9,1 g</p>
            <p>Waarvan suikers: 8,9 g</p>
            <p>Eiwitten: 1,9 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>
    <hr>
    <div class="row padding-extra background-horeca align-text-left margin-last-item">
        <div class="col-md-4">
            <img src="assets/img/12-horeca.jpg" class="horeca-images-resize" alt="...">
        </div>
        <div class="col-md-4">
            <h3>Appel 0,33 liter</h3>
            <p>De meest gegeten appel van Nederland is de Elstar. Uit de hand vinden wij die ook het lekkerst, maar wij
                hebben gemerkt dat als je er ook Jonagold en nog een paar verrassende appelsoorten bij het sap doet, het
                sap een frisse en nog lekkerdere smaak krijgt. Gelukkig groeien deze appels ook in de boomgaarden in de
                Flevopolder.</p>
        </div>
        <div class="col-md-4">
            <p class="font-weight-bold">Voedingswaarde per 100 gram</p>
            <p>Energie: 194 kJ (46 kcal)</p>
            <p>Vetten: 0,0 g</p>
            <p>Waarvan verzadigd: 0,0 g</p>
            <p>Koolhydraten: 11,2 g</p>
            <p>Waarvan suikers: 10,5 g</p>
            <p>Eiwitten: 0,1 g</p>
            <p>Zout: 0,0 g</p>
        </div>
    </div>

</div>

</body>
</html>
