<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Order</title>
</head>

<div class="container-fluid mt-5 p-3 rounded cart">
    <div class="row no-gutters">
        <div class="col-md-5">
            <div class="product-details mr-1.5">
                <div class="d-flex justify-content-between">
                    <span>
                        <h3>Uw producten: </h3>
                    </span>
                </div>
                <hr>
                <?php
                if (isset($cart)) {
                    $price = 0;
                    foreach ($cart as $cartItem):
                        $quantity = $cartItem['quantity'];
                        $product = $cartItem['product'];
                        //$a so people can't buy the entire stock (reason: competitor monopoly)
                        $a = $product->getStock() / 2; ?>
                        <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded">
                            <div class="d-flex flex-row">
                                <img src='assets/img/<?= $product->getId(); ?>.jpg' class='rounded' alt='img' width='100'>
                                <div class="ml-2"><span class="font-weight-bold d-block">Naam: <?= $product->getName(); ?></span>
                                    <span class="spec"></span>
                                </div>
                            </div>
                            <div class="d-flex flex-row align-items-center"><span class="font-weight-bold d-block">Aantal: <?= $quantity?></span>
                                <span class="d-block ml-5 font-weight-bold">Prijs: €<?= number_format($product->getPrice()*$quantity, 2); ?></span>
                            </div>
                        </div>
                        <div></div>
                <?php endforeach;
                    }

                $price = $_POST['price'];
                    echo "<h4><strong>Totaal: ". number_format((double)$price,2)."</strong></h4>";
                ?>
                <div id="return-button">
                    <button type="button" onclick="window.location.href='winkelwagen';" class="btn btn-primary">Terug naar winkelwagen</button>
                </div>
                <div id="pay-button">
                    <form action="betalen" method="post">
                        <input type="hidden" name="price" value='<?= $price ?>'>
                        <button type="submit" class="btn btn-primary" name="Betalen" >Betalen</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>