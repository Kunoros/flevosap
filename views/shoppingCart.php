<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../assets/css/winkelwagen.css">
    <title>Winkelwagen</title>

</head>
<body>
<div class="container-fluid mt-5 p-3 rounded cart">
    <div class="row no-gutters">
        <div class="col-md-7">
            <div class="product-details mr-1.5">
                <div class="d-flex justify-content-between">
                    <span>
<!--                        Count and grammar for the shopping cart-->
                        <?php
                        $count= 0;
                        foreach ($cart as $cartItem):
                        $product = $cartItem['product'];
                        $quantity = $cartItem['quantity'];
                         if ($product->getStock() >= $quantity):
                         $count += $quantity;
                         endif;
                        endforeach;
                        ?>
                        <h3>U heeft <?php if ($count > 0):echo count($cart);
                                echo " product"; endif;
                            if ($count > 1):echo "en"; endif;
                            if ($count < 1):echo "nog geen producten";endif; ?> in uw winkelwagen
                        </h3>
                    </span>
                </div>
                <hr>

                <?php if (isset($cart)):
                    $price = 0;
                    foreach ($cart as $cartItem):
                        $quantity = $cartItem['quantity'];
                        $product = $cartItem['product'];
                        //$a so people can't buy the entire stock (reason: competitor monopoly)
                        if ($product->getStock() == 0):
                            echo "Dit product is niet op voorraad";
                        else:
                            $a = round($product->getStock() / 2); ?>
                            <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded">
                                <div class="d-flex flex-row">
                                    <img src='assets/img/<?= $product->getId(); ?>.jpg' class='rounded' alt='img'
                                         width='100'>
                                </div>
                                <div class="ml-2"><span class="font-weight-bold d-block"
                                                        id="name">Naam: <?= $product->getName(); ?> </span>
                                    <span class="spec"></span>
                                </div>
                                <span class="font-weight-bold d-block">Aantal: </span>
                                <div class="d-flex flex-row align-items-center" id="amountAlign">
                                    <form action="winkelwagen-update" method="post">
                                        <input type="hidden" name="productid" value="<?= $product->getId() ?>">
                                        <input type="number" id="quantity" name="quantity" min="1" max="<?= $a; ?>"
                                               value="<?= $quantity; ?>">
                                        <button type="submit" class="btn btn-primary" name="update">Update</button>
                                    </form>
                                </div>
                                <div class="d-flex flex-row align-items-center" id="deleteAlign">
                                    <form action="winkelwagen-remove" method="post">
                                        <input type="hidden" name="productid" value="<?= $product->getId() ?>">
                                        <button type="submit" class="btn btn-danger" name="remove">Remove</button>
                                    </form>
                                </div>
                                <span class="d-block ml-5 font-weight-bold">Prijs: €<?= number_format($product->getPrice() * $quantity, 2); ?></span>
                            </div>

                            <?php $price = $price + $product->getPrice() * $quantity;
                        endif;
                    endforeach;
                    if ((count($_SESSION['cart'])) != 0):

                        echo "<h4 class='d-flex flex-row align-items-center'>Totaal: €" . number_format($price, 2) . "</h4>";
                        if ($price < 20):echo "U moet minimaal 20,00 euro aan producten hebben om een bestelling te plaatsen.";
                        else:?>
                            <form action="bestel" method="post" id="order">
                                  <input type="hidden" name="price" value= <?= $price ?>>
                                  <button type="submit" class="btn btn-primary" name="order">Bestel</button>
                            </form>
                        <?php
                            endif;
                        endif;
                endif ?>

            </div>
        </div>
    </div>
</div>
</body>
</html>