function postData(url = '', data = {}, options = {}) {
    return new Promise(((resolve, reject) => {
        var xhttp;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            console.log(this);
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status <= 299) {
                    return resolve(this)
                } else {
                    return reject(this);
                }
            }
        }
        xhttp.open("POST", url, true)
        xhttp.send(JSON.stringify(data));
    }))
}