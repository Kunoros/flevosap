function removeProduct(productId) {
    if (confirm("Weet u zeker dat u dit product wilt verwijderen")) {
        postData('remove-product', {productId}).then(result => {
            document.getElementById('product_id_' + productId).remove();
        })
    }
}

function updateProduct(productId) {
    var row = document.getElementById('product_id_' + productId);
    var product = {
        id: productId
    };
    Array.prototype.slice.call(row.getElementsByClassName('form-control')).forEach(function (input) {
        if (input.name) {
            product[input.name] = input.value;
        }
    })


    let file = document.getElementById('file_upload_' + productId).files[0];

    if (file && row.classList.contains('changed-img')) {
        uploadPhoto(productId, file);
        row.classList.remove('changed-img')
    }

    if (row.classList.contains('changed')) {
        postData('update-product', product).then(result => {
            row.classList.add('updated')
        }).catch(error => {
            row.classList.add('failed')
        }).finally(() => {
            row.classList.remove('changed')
        })
    }
}

const rows = document.getElementsByClassName("productInput");
Array.prototype.slice.call(rows).forEach(function (row) {
        // observer.observe(row, {childList: true, subtree: true, characterData: true})
        row.addEventListener('change', function (event) {
            changeEvent(event, row)
        })
    }
)

function changeEvent(event, row) {
    if (event.target.name === "IMAGE") {

        let file = event.target.files[0];
        let image = row.querySelector(".image")
        image.src = URL.createObjectURL(file);
        row.classList.add('changed-img')
    } else {
        row.classList.add('changed');
    }
    row.classList.remove('updated');
    row.classList.remove('failed');
}

{

}

function cancelImage(productId) {
    let row = document.getElementById('product_id_' + productId);
    document.getElementById('file_upload_' + productId).value = '';
    row.querySelector('.image').src = "product/image?id=" + productId;
    row.classList.remove('changed-img')
}

function uploadPhoto(productId, file) {
    var formData = new FormData();
    formData.append('file', file);
    formData.append('id', productId);
    return new Promise(((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            console.log(this);
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status <= 299) {
                    return resolve(this)
                } else {
                    return reject(this);
                }
            }
        }
        xhr.open("POST", 'product/image', true)
        xhr.send(formData);
    }))
}

function addProduct() {
    var row = document.getElementById('product_row_body').getElementsByClassName('row productInput');
    let clone = row[0].cloneNode(true);
    Array.prototype.slice.call(clone.getElementsByClassName('form-control')).forEach(function (input) {
        if (input.name) {
            if (input.type === "number") {
                input.value = 0;
            } else {
                input.value = "";
            }
        }
    })
    Array.prototype.slice.call(clone.getElementsByClassName('nutrition-form-ctrl')).forEach(function (input) {
        if (input.name) {
            if (input.type === "number") {
                input.value = 0;
            } else {
                input.value = "";
            }
        }
    })
    clone.classList.add('changed')
    clone.setAttribute('productid', 'new')
    clone.id = "product_id_new";

    let image = clone.querySelector(".product-images>.image");
    image.src = "assets/img/flevosap-logo.png"

    let deleteButton = clone.querySelector(".actionWrapper > .remove.clickable")
    deleteButton.removeAttribute("onclick");
    deleteButton.addEventListener('click', (event) => {
        if (confirm("Wil u dit nieuwe product maken stoppen?")) {
            clone.remove()
        }
    })

    let saveButton = clone.querySelector('.actionWrapper > .save.clickable');
    saveButton.removeAttribute("onclick");
    saveButton.addEventListener('click', saveProduct)

    clone.querySelector(".product-images > .imageUpload > label.uploahdLabel").setAttribute('for', 'file_upload_new')
    let imageInput = clone.querySelector(".product-images > .imageUpload > .imageInput")
    imageInput.id = "file_upload_new"
    imageInput.addEventListener('change', (event) => {
        let file = event.target.files[0];
        image.src = URL.createObjectURL(file);
        clone.classList.add('changed-img')
    })

    document.getElementById('product_row_body').appendChild(clone);
    clone.scrollIntoView();
}

function saveProduct() {
    let row = document.getElementById('product_id_new');
    let product = {
        id: productId
    };
    Array.prototype.slice.call(row.getElementsByClassName('form-control')).forEach(function (input) {
        if (input.name) {
            product[input.name] = input.value;
        }
    })


}