CREATE OR REPLACE DATABASE Flevosap_b3;
# control enter is een run van de line
# ctrl alt l voor beautifier

USE Flevosap_b3;

SHOW TABLES;

CREATE OR REPLACE TABLE products
(
    id          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name        VARCHAR(64)     NOT NULL,
    description TEXT            NOT NULL DEFAULT '',
    price       DECIMAL(4,2),
    stock       INT,
    image       LONGBLOB,
    imageType   VARCHAR(64),
    createdAt   DATETIME                 DEFAULT NOW(),
    updatedAt   DATETIME                 DEFAULT NOW()
);

#All products on Flevosap.nl ordered by wether or not they come in a bottle or package.
INSERT INTO products(name,price,stock,description)
VALUES ('Appel',2.50,10000,'De meest gegeten appel van Nederland is de Elstar. Uit de hand vinden wij die ook het lekkerst, maar wij hebben gemerkt dat als je er ook Jonagold en nog een paar verrassende appelsoorten bij het sap doet, het sap een frisse en nog lekkerdere smaak krijgt. Gelukkig groeien deze appels ook in de boomgaarden in de Flevopolder.'),
       ('Kleintje Appel',2.50,10000,'De pet fles (0,33 liter) is alleen verkrijgbaar in de smaak appel. En makkelijk mee te nemen voor onderweg. De meest gegeten appel van Nederland is de Elstar. Wij hebben gemerkt dat als je er ook Jonagold en nog een paar verrassende appelsoorten bij het sap doet, het sap een frisse en nog lekkerdere smaak krijgt.'),
       ('Appel Aardbei',2.50,10000,'Flevosap Appel Aardbei is een heerlijk zomer sap. Zomerkoninkjes – zoals aardbeien ook wel genoemd worden –  zijn de snoepjes van de natuur. Het zoete zomerse van de aardbei gecombineerd met het frisse van appel heeft een smaak die iedereen wel ziet zitten.'),
       ('Appel Ananas Perzik',2.50,10000,'Bij Flevosap houden we wel van een feestje. Vele vruchten zijn al in huis. Wie zullen we nog meer uitnodigen? Natuurlijk, ananas en perzik! Samen met appel maken we er een fantastisch feest van.'),
       ('Appel Cassis',2.50,10000,'Wij wilden ook een rood sap hebben. Dat kan met kers en met cassisbessen. Dus hebben ze beiden geprobeerd. Het zag er meteen goed uit. En de smaak was super. Maar welke is nou lekkerder? Daar kwamen ze bij de familie Vermeulen niet uit. Dit is de Appel Cassis, de anderen rode Flevosap is Appel Kers.'),
       ('Appel Citroen',2.50,10000,'Klaar met beachvolleybal, of gewoon gefitnesst? Dan heb je toch zin in fris, frisser, frist? Flevosap is altijd fris, Flevosap Appel Citroen is frisser en ijskoud is dit sap het aller frist. Daar fris je van op.'),
       ('Appel Cranberry',2.50,10000,'Fris zoet/zuur, veel smaak, goed voor je. Het zit allemaal in deze fles. Onze favoriete appelsap gemixt met puur cranberry, ook wel veenbes genoemd. Wat maakt deze bes nou zo bijzonder? Hij zit boordevol belangrijke voedingstoffen die goed voor je zijn en mogelijk een positief effect kunnen hebben op (blaas)ontsteking'),
       ('Appel Kers',2.50,10000,'Wij wilden ook een rood sap hebben. Kersen of zwarte bessen zijn dan de voor de hand liggende kleur en dus ook smaakbepalers. We hebben beide geprobeerd. Het zag er meteen goed uit. En de smaak was super. Maar welke is nou lekkerder? Daar kwamen we niet uit, dus hebben we beiden gemaakt. Dit is de Appel-Kers, de andere rode Flevosap is Appel Cassis.'),
       ('Appel Peer',2.50,10000,'Je moet appels niet met peren vergelijken, zegt men. Maar van beide sap maken kan wel heel goed, zeker als je beide vruchten zelf teelt. Naast de zoete smaak van puur perensap, zorgt de toevoeging van appel voor de frisheid. Vind je deze sap ook een onvergelijkbare smaak hebben?'),
       ('Appel Peer Zwarte Bes',2.50,10000,'Deze drie zijn samen onweerstaanbaar. Het aantrekkelijke sap van het donkere vruchtvlees van de zwarte bes, gecombineerd met het sap van zelf geteelde appels en peren. Zo ontstond deze combinatie van drie vruchten. Lekker kleurtje, lekker sapje.'),
       ('Appel Rabarber',2.50,10000,'Tussen de schuur en de boomgaard is nog een kleine moestuin. Daar groeit rabarber. Elk jaar weer. Gek, hoe je elke dag langs zoiets lekkers kan lopen en het niet ziet staan. Maar als je het eenmaal ziet … We hebben de dikke dieprode stelen samen met onze appels geperst tot een appelsap met de unieke smaak van rabarber er doorheen.'),
       ('Appel Sinaasappel',2.50,10000,'De appel en de sinaasappel als vrucht kruizen is tot nu toe niemand gelukt. Maar als sap is dat niet zo ingewikkeld, zou je denken. We hebben er toch lang over gedaan om de juiste balans te vinden tussen het zoete van de sinaasappel en het frisse van appel. Een heerlijk gezonde dorstlesser met een hele zachte smaak.'),
       ('Peer',2.50,10000,'Een gevoelig typje. Er staan ook perenbomen in onze boomgaard in Flevoland. Net als onze appels zijn deze peren bedoeld om gewoon te eten. Zulke lekkere sappige peren dat het logisch is dat we daar ook lekker sap van maken. Onze peren zijn echt gevoelige typetjes, daarom een vleugje citroensap erbij om de snelle bruin-kleuring tegen te gaan. Perensap is lekker voor elk moment van de dag.'),
       ('Peer Cranberry',2.50,10000,'Tegenpolen trekken elkaar aan, zegt men wel eens. Dat geldt zeker voor peer en cranberry – de meest zoete vrucht en de meest zure vrucht. Doordat wij de zoete en zure vruchten gingen mengen ontstond een perfecte zachte smaak sap. Onverwacht lekker.'),
       ('Sinaasappel',2.50,10000,'Sinaasappelsap is het meest gedronken sap. Ook bij de familie Vermeulen werd wel eens een sinaasappeltje geperst voor bij het ontbijt. Van het een kwam het ander. Wat begon met een handgeperst sapje bij het ontbijt werd sap voor de buurt, voor de Flevopolder en omdat het zo lekker is, voor heel Nederland.'),
       ('Winteravond',2.50,10000,'Wie aan de winter denkt, denkt aan koek en zopie. Toch? En dat doet denken aan warme chocolademelk en glühwein. De combinatie chocolademelk en appelsap leek ons geen goed idee, dus zijn we gaan experimenteren met appelsap en glühweinspecerijen. Zo ontstond deze wintersmaak met een kruidige afdronk. Heerlijk zo uit de fles, maar warm* nog lekkerder.   *Verwarm in pan of magnetron, het sap mag niet gaan koken.'),
       ('Rode Biet',2.50,10000,'Er is hier voldoende ruimte om alle voordelen van rode bieten op te sommen. Waar ze vandaan komen en hoe ze smaken evenmin. We maken er liever sap van. Met in gedachten dat een lekkere biet hier vanzelf boven uitstijgt.'),
       ('Tomaat',2.50,10000,'Tomatensap is rijk aan lycopeen, een super-antioxidant die je immuumsysteem versterkt, en dat is fijn. Maar het zijn de prachtige rood glimmende, rijp geplukte tomaten met hun unieke lichtzoete, grasachtige geur, die ons meteen doen denken aan de zomer bij zonsopkomst. Geniet van de heerlijke diepe smaak.'),
       ('Wortel',2.50,10000,'Zacht zoet en een beetje bitter, maar niet bitterzoet. Hoe zou jij de bijzondere smaak van de oranje bospeen omschrijven? Die smaak komt, net als de worteltjes zelf, uit de grond. En dat is toch heel bijzonder! Wat proef jij in dit heerlijke wortelsap?'),
       ('Appel-Braam',2.50,10000,'Wij zijn dol op het proberen van nieuwe smaakcombinaties. We hebben hier onze favoriete appel gemengd met pure bramen. Deze mix is werkelijk een rijke aanvulling, die je echt moet proberen!'),
       ('Appel',2.50,10000,'De meest gegeten appel van Nederland is de Elstar en uit de hand vinden wij die ook het lekkerst. Maar wij hebben gemerkt dat als je er ook Jonagold en nog een paar verrassende appelsoorten bij doet, het sap een nog frissere smaak krijgt.'),
       ('Appel Ananas Perzik',2.50,10000,'Bij Flevosap houden we wel van een feestje. Vele vruchten zijn al in huis. Wie zullen we nog meer uitnodigen? Natuurlijk, ananas en perzik! Samen met appel maken we er een fantastisch feest van.');

SELECT *
FROM products;

CREATE OR REPLACE TABLE nutrition
(
    id            INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    productId     INT             NOT NULL,
    energyKj      INT             NOT NULL,
    energyKcal    INT             NOT NULL,
    fats          FLOAT           NOT NULL,
    saturatedFats FLOAT           NOT NULL,
    carbohydrates FLOAT           NOT NULL,
    sugars        FLOAT           NOT NULL,
    protein       FLOAT           NOT NULL,
    salt          FLOAT           NOT NULL,
    createdAt     DATETIME DEFAULT NOW(),
    updatedAt     DATETIME DEFAULT NOW(),
    FOREIGN KEY (productId) REFERENCES products (id) ON DELETE CASCADE
);

#All nutrition values for the products on Flevosap.nl.
INSERT INTO nutrition (productid, energykj, energykcal, fats, saturatedfats, carbohydrates, sugars, protein, salt,
                       updatedat)
VALUES (1, 194, 46, 0.0, 0.0, 11.2, 10.5, 0.1, 0.0, NOW()),
       (2, 194, 46, 0.0, 0.0, 11.2, 10.5, 0.1, 0.0, NOW()),
       (3, 198, 47, 0.0, 0.0, 11.4, 10.6, 0.2, 0.0, NOW()),
       (4, 194, 46, 0.1, 0.0, 11.1, 10.4, 0.3, 0.0, NOW()),
       (5, 200, 47, 0.0, 0.0, 11.3, 10.7, 0.1, 0.0, NOW()),
       (6, 187, 44, 0.0, 0.0, 10.7, 10.0, 0.1, 0.0, NOW()),
       (7, 180, 43, 0.0, 0.0, 10.6, 9.2, 0.1, 0.0, NOW()),
       (8, 205, 49, 0.1, 0.0, 11.6, 10.8, 0.2, 0.0, NOW()),
       (9, 172, 41, 0.0, 0.0, 10.0, 9.6, 0.1, 0.0, NOW()),
       (10, 181, 42, 0.0, 0.0, 10.2, 9.9, 0.1, 0.0, NOW()),
       (11, 156, 37, 0.1, 0.0, 8.8, 8.1, 0.2, 0.0, NOW()),
       (12, 192, 46, 0.0, 0.0, 10.5, 9.9, 0.7, 0.1, NOW()),
       (13, 150, 35, 0.0, 0.0, 8.7, 8.7, 0.1, 0.0, NOW()),
       (14, 145, 34, 0.0, 0.0, 8.6, 7.8, 0.1, 0.0, NOW()),
       (15, 188, 45, 0.1, 0.0, 9.1, 8.9, 1.9, 0.3, NOW()),
       (16, 194, 46, 0.0, 0.0, 11.2, 10.5, 0.1, 0.0, NOW()),
       (17, 131, 31, 0.0, 0.0, 6.8, 6.8, 0.6, 0.1, NOW()),
       (18, 115, 27, 0.0, 0.0, 5.0, 3.8, 1.2, 0.0, NOW()),
       (19, 107, 25, 0.0, 0.0, 5.3, 5.3, 0.5, 0.1, NOW()),
       (20, 214, 51, 0.2, 0.0, 11.2, 10.3, 0.3, 0.0, NOW()),
       (21, 194, 46, 0.0, 0.0, 11.2, 10.5, 0.1, 0.0, NOW()),
       (22, 194, 46, 0.1, 0.0, 11.1, 10.4, 0.3, 0.0, NOW());

SELECT *
FROM nutrition;

CREATE OR REPLACE TABLE users
(
    id           INT PRIMARY KEY                        NOT NULL AUTO_INCREMENT,
    firstName    VARCHAR(64)                            NOT NULL,
    lastName     VARCHAR(64)                            NOT NULL,
    email        VARCHAR(64)                            NOT NULL,
    passwordHash VARCHAR(64)                            NOT NULL,
    street       VARCHAR(64)                            NOT NULL,
    houseNumber  INT(5)                                 NOT NULL,
    postalCode   VARCHAR(6)                             NOT NULL,
    city         VARCHAR(64)                            NOT NULL,
    role         ENUM ('CUSTOMER', 'EMPLOYEE', 'ADMIN') NOT NULL DEFAULT 'CUSTOMER',
    createdAt    DATETIME                                        DEFAULT NOW(),
    updatedAt    DATETIME                                        DEFAULT NOW()
);
-- email test@email.com     password: test
INSERT INTO users (id, firstName, lastName, email, passwordHash, street, houseNumber, postalCode, city)
VALUES (1, 'test', 'last', 'test@email.com', '$2y$10$wVs.AbVllLaHSzgJ1Nzl0uZzg8.Zb3ku4.HJMlouzBCX7lP4evyE2', 'magic',
        '420', '1234aa', 'neverland');

-- email: admin@email.com   password: admin
INSERT INTO users (id, firstName, lastName, email, passwordHash, street, houseNumber, postalCode, city, role)
VALUES (2, 'Harm', 'Admin', 'admin@email.com', '$2y$10$d0InhOM5EE8z5lxKlZ60YODmQhPU0e4pwjChFX9uIlNzsVsP89JqW',
        'company', '1', '1234ab', 'neverland', 'ADMIN');



SELECT *
FROM users;

SELECT *
FROM products;

SELECT *
FROM nutrition;

CREATE OR REPLACE TABLE shoppingCart
(
    id        INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    userId    INT             NOT NULL,
    createdAt DATETIME DEFAULT NOW(),
    updatedAt DATETIME DEFAULT NOW(),
    FOREIGN KEY (userId) REFERENCES users (id)
);

INSERT INTO shoppingCart (id, userId)
VALUES (1, 1);
INSERT INTO shoppingCart (id, userId)
VALUES (2, 1);
INSERT INTO shoppingCart (id, userId)
VALUES (3, 1);

CREATE OR REPLACE TABLE cartsProducts
(
    cartId    INT NOT NULL,
    productId INT NOT NULL,
    amount    INT NOT NULL DEFAULT 1,
    PRIMARY KEY (cartId, productId),
    FOREIGN KEY (productId) REFERENCES products (id) ON DELETE CASCADE,
    FOREIGN KEY (cartId) REFERENCES shoppingCart (id)
);

INSERT INTO cartsProducts (cartId, productId, amount)
VALUES (1, 1, 4);
INSERT INTO cartsProducts (cartId, productId, amount)
VALUES (1, 2, 8);
INSERT INTO cartsProducts (cartId, productId, amount)
VALUES (1, 3, 7);
INSERT INTO cartsProducts (cartId, productId, amount)
VALUES (2, 4, 4);
INSERT INTO cartsProducts (cartId, productId, amount)
VALUES (2, 1, 1);
INSERT INTO cartsProducts (cartId, productId, amount)
VALUES (2, 3, 4);
INSERT INTO cartsProducts (cartId, productId, amount)
VALUES (3, 1, 9);

