<?php

class orderlist extends BaseController
{
    public function getOrderlistView()
    {
        $orders = Order::getAll();
        $this->RenderView('orderlist', ['orders' =>$orders]);
    }
}