<?php

class betalen extends BaseController {

    public function getBetalenView()
    {
        $this->RenderView('betalen',['cart' => $_SESSION['cart']]);
    }
    public function order()
    {
        if (isset($_POST['pay'])) {
            $user = $_SESSION['user'];
            $cart = $_SESSION['cart'];
            Order::placeOrder($user,$cart);
        }
        $_SESSION['alert'] = true;
        header('Location: ./');
    }
}