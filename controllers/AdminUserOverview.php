<?php


class AdminUserOverview extends BaseController implements View
{

    public function loadView()
    {
        $users = User::getAll();
        $this->RenderView('admin.userOverview', ['users' => $users]);
    }
}