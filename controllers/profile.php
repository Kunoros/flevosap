<?php

class profile extends BaseController {

    public function getProfileView()
    {
        $orderHistories = Order::getOrderHistory();
        $this->RenderView('profile', ['orderHistories' => $orderHistories]);
    }

    public function profileUpdate() {
        $user = User::updateUser($_POST);
        if ($user && isset($_POST['submit'])) {
            if ($user->getId()==$_SESSION['user']->getId()) {
                $_SESSION['user'] = $user;
            }
            header('Location: ./profile?msg=Profile updated&type=success');
        }
    }
}