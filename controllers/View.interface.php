<?php
interface View
{
    public function loadView();
}