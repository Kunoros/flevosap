<?php

class products extends BaseController
{
    public $result = [];

    public function getSearch()
    {
        $results = Product::search('%' . $_POST["search"] . '%');
        $this->RenderView("webshop", ['products' => $results]);

    }

    public function getProductView()
    {
        $url = $_SERVER['REQUEST_URI'];

        parse_str(parse_url($url)['query'], $query);
        $result = Product::getProduct($query['id']);
        $this->RenderView('products', ['product' => $result]);
    }

    public function getImage()
    {
        $image = Product::getImageBlob($_GET['id']);
        if ($image) {
            header("Content-Type: " . $image['contentType']);
            echo($image['lob']);
        }
        else {
            header("Content-Type: image/png");
            readfile('assets/img/flevosap-logo.png');
        }
    }
}