<?php


class adminProducts extends BaseController implements View
{
    public function loadView()
    {
        $products = Product::getProducts();
        $this->RenderView('admin.products', ['products' => $products]);
    }

    public function removeProduct() {
        $data = json_decode(file_get_contents('php://input'), true);

        if(Product::delete($data['productId'])){
            http_response_code(204);
        }
        else {
            http_response_code(404);
        }
    }

    public function updateProduct()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $product = Product::getProduct(intval($data['id']));
        $product->setDescription($data['description']);
        $product->setName($data['name']);
        $product->setPrice(floatval($data['price']));
        $product->setStock(intval($data['stock']));
        if($product->update()){
        }
        else {
            http_response_code(500);
        }
    }

    public function createProduct()
    {
        $data = json_decode(file_get_contents('php://input'), true);

    }

    public function updateImage()
    {
        Product::uploadImage($_FILES['file']['tmp_name'], $_FILES['file']['type'], $_POST['id']);
    }
}