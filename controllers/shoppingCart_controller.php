<?php

class shoppingCart_controller extends BaseController
{

    public $products = [];

    public function getShoppingCartView()
    {
        $this->RenderView('shoppingCart',['cart' => $_SESSION['cart']]);
    }

    public function shoppingCart()
    {
        $id = intval($_POST['productid']);
        if (isset($_SESSION['cart'][$id])) {
            $cartItem = $_SESSION['cart'][$id];
            $cartItem['quantity']++;
        } else {
            $cartItem = [
                'quantity' => 1,
                'product' => Product::getProduct($id)
            ];
        }
        $_SESSION['cart'][$id] = $cartItem;
        header('Location: ./winkelwagen');
    }

    public function updateShoppingCart(){
        $id = intval($_POST['productid']);
        $quantity = intval($_POST['quantity']);
        if (isset($_SESSION['cart'][$id])) {
            $cartItem = $_SESSION['cart'][$id];
            $cartItem['quantity'] = $quantity;
            $_SESSION['cart'][$id] = $cartItem;
        }
        header('Location: ./winkelwagen');
    }

    public function removeShoppingCartItem(){
        $id = intval($_POST['productid']);
        if (isset($_SESSION['cart'][$id])) {
            unset($_SESSION['cart'][$id]);
        }
        header('Location: ./winkelwagen');
    }
}