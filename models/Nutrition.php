<?php


class Nutrition
{
    private int $id, $productId;
    private int $energyKj, $energyKcal;
    private float $fats, $saturatedFats, $carbohydrates, $sugars, $protein, $salt;
    private DateTime $createdAt, $updatedAt;





    /**
     * Nutrition constructor.
     * @param int $id
     * @param int $productId
     * @param int $energyKj
     * @param int $energyKcal
     * @param float $fats
     * @param float $saturatedFats
     * @param float $carbohydrates
     * @param float $sugars
     * @param float $protein
     * @param float $salt
     * @param DateTime $createdAt
     * @param DateTime $updatedAt
     */
    public function __construct(int $id, int $productId, int $energyKj, int $energyKcal, float $fats, float $saturatedFats, float $carbohydrates, float $sugars, float $protein, float $salt, DateTime $createdAt, DateTime $updatedAt)
    {
        $this->id = $id;
        $this->productId = $productId;
        $this->energyKj = $energyKj;
        $this->energyKcal = $energyKcal;
        $this->fats = $fats;
        $this->saturatedFats = $saturatedFats;
        $this->carbohydrates = $carbohydrates;
        $this->sugars = $sugars;
        $this->protein = $protein;
        $this->salt = $salt;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getEnergyKj(): int
    {
        return $this->energyKj;
    }

    /**
     * @return int
     */
    public function getEnergyKcal(): int
    {
        return $this->energyKcal;
    }

    /**
     * @return float
     */
    public function getFats(): float
    {
        return $this->fats;
    }

    /**
     * @return float
     */
    public function getSaturatedFats(): float
    {
        return $this->saturatedFats;
    }

    /**
     * @return float
     */
    public function getCarbohydrates(): float
    {
        return $this->carbohydrates;
    }

    /**
     * @return float
     */
    public function getSugars(): float
    {
        return $this->sugars;
    }

    /**
     * @return float
     */
    public function getProtein(): float
    {
        return $this->protein;
    }

    /**
     * @return float
     */
    public function getSalt(): float
    {
        return $this->salt;
    }
}