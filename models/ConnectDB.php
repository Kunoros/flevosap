<?php

class ConnectDB {

    public static function connectDB() {
        $hostName = 'localhost';
        $dbName = 'Flevosap_b3';
        $userName = 'root';
        $password = 'root';


        try {
            return new PDO("mysql:host=$hostName;dbname=$dbName", "$userName", "$password", []);
        } catch (PDOException $e) {
            echo 'Failed to connect to database';
        }
    }
}
