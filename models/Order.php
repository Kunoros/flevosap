<?php

class Order extends ConnectDB
{
    private int $id;
    private int $userId;
    private DateTime $createdAt;
    private DateTime $updatedAt;
    private User $user;
    private array $products;
    private float $totalPrice;

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    public function __construct(int $id, int $userId, User $user, DateTime $createdAt, DateTime $updatedAt, array $products)
    {
        $this->userId = $userId;
        $this->id = $id;
        $this->user = $user;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->products = $products;
    }

    public static function placeOrder($user, $cart)
    {
        $dbConn = self::connectDB();
        $query = "INSERT INTO shoppingCart (userId) VALUE (:userId)";
        $stm = $dbConn->prepare($query);
        $stm->execute(['userId' => $user->getId()]);

        $id = $dbConn->lastInsertId();
        $query = "INSERT INTO cartsProducts (cartId, productId, amount) VALUES (:cartId, :productId, :amount)";

        $stm = $dbConn->prepare($query);
        foreach ($cart as $product) {
            $newProduct = [
                'cartId' => $id,
                'productId' => $product['product']->getId(),
                'amount' => $product['quantity']
            ];
            $stm->execute($newProduct);
        }
    }

    public static function getAll()
    {
        $dbConn = self:: connectDB();
        $query = "
            SELECT *, users.id AS user_id, shoppingCart.id AS order_id FROM shoppingCart
            JOIN users ON users.id = shoppingCart.userId
        ";
        $stm = $dbConn->prepare($query);
        $stm->execute();
        $results = $stm->fetchAll(PDO::FETCH_ASSOC);
        $returnarray = [];

        $productQuery = '
            SELECT cP.cartId AS cart_id, products.id AS product_id, n.id AS nutrition_id, products.*, n.*, cP.* FROM products
            JOIN cartsProducts cP on products.id = cP.productId
            JOIN nutrition n on products.id = n.productId
        ';

        $productStm = $dbConn->prepare($productQuery);
        $productStm->execute();
        $products = $productStm->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_ASSOC);

//        echo '<pre>';
//        die(var_dump($products));
        foreach ($results as $result) {
            $productsArray = [];
            if (isset($products[$result['order_id']])) {
                foreach ($products[$result['order_id']] as $product) {
                    $productsArray[] = [
                        'product' => new Product(
                            intval($product['product_id']),
                            intval($product['stock']),
                            $product['name'],
                            $product['description'],
                            floatval($product['price']),
                            new DateTime($product['createdAt']),
                            new DateTime($product['updatedAt']),
                            new Nutrition(
                                intval($product['nutrition_id']),
                                intval($product['product_id']),
                                intval($product['energyKj']),
                                intval($product['energyKcal']),
                                floatval($product['fats']),
                                floatval($product['saturatedFats']),
                                floatval($product['carbohydrates']),
                                floatval($product['sugars']),
                                floatval($product['protein']),
                                floatval($product['salt']),
                                new DateTime($product['createdAt']),
                                new DateTime($product['updatedAt']),
                            )
                        ),
                        'amount' => $product['amount']
                    ];
                }
            }
            $neworder = new Order (
                intval($result['order_id']),
                intval($result['userId']),
                new User(
                    $result['user_id'],
                    $result['firstName'],
                    $result['lastName'],
                    $result['email'],
                    $result['street'],
                    $result['houseNumber'],
                    $result['postalCode'],
                    $result['city'],
                    $result['role']),
                new DateTime($result ['createdAt']), new DateTime($result['updatedAt']),
                $productsArray
            );

            $returnarray[] = $neworder;
        }
        return $returnarray;

    }

//    public static function getCartId($id) {
//        $dbConn = self::connectDb();
//        $stm = $dbConn->prepare("SELECT id FROM shoppingcart WHERE userId = :id");
//        $stm->bindParam(':id', $id);
//        if ($stm->execute() && $stm->rowCount() > 0) {
//            $cartId = $stm->fetch();
//            print_r($cartId);
//        }
//
//    }

    public static function getOrderHistory()
    {

        $userId = $_SESSION['user']->getId();
        $dbConn = self::connectDB();

        $query = "SELECT * FROM shoppingCart
                JOIN cartsProducts ON cartId = shoppingCart.id
                JOIN products ON cartsProducts.productId = products.id
                WHERE userId = $userId
                  ";

        $stm = $dbConn->prepare($query);
        $stm->execute();
        $orders = $stm->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_ASSOC);
        $finalPrice = 0.00;
        $ordersArray = [];

        foreach ($orders as $key => $order) {
            foreach ($order as $product) {
//                die(var_dump($product));
                $productPrice = $product['price'] * $product['amount'];
                $finalPrice = $finalPrice + $productPrice;
            }
            $ordersArray[$key]['cartId'] = $order[0]['cartId'];
            $ordersArray[$key]['finalPrice'] = number_format($finalPrice, 2);
            $ordersArray[$key]['createdAt'] = $order[0]['createdAt'];
        }
        return $ordersArray;
    }


    public function formatId(): string
    {
        return sprintf('#%06d', $this->id);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }
}