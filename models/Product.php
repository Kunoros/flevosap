<?php

class Product extends ConnectDB
{
    private int $id, $stock;
    private string $name;
    private string $description;
    private float $price;
    private DateTime $createdAt, $updatedAt;
    private Nutrition|null $nutrition;

    /**
     * Product constructor.
     * @param int $id
     * @param int $stock
     * @param string $name
     * @param string $description
     * @param float $price
     * @param DateTime $createdAt
     * @param DateTime $updatedAt
     * @param Nutrition|null $nutrition
     */
    public function __construct(int $id, int $stock, string $name, string $description, float $price, DateTime $createdAt, DateTime $updatedAt, Nutrition|null $nutrition)
    {
        $this->id = $id;
        $this->stock = $stock;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->nutrition = $nutrition;
    }

    public function update()
    {
        $db = self::connectDB();
        $query = "
            UPDATE products SET
                stock = :stock,
                name = :name,
                description = :description,
                price = :price,
                updatedAt = NOW()
            WHERE id= :id
        ";
        $stm = $db->prepare($query)->execute([
            'id' => $this->id,
            'stock' => $this->stock,
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price
        ]);
        if ($stm->rowCount()) {
            return true;
        } else return false;
    }

    public static function getProduct(int $id): Product
    {
        $db = self::connectDB();

        $query = "SELECT *, products.id as product_id, nutrition.id as nutrition_id FROM products JOIN nutrition on products.id = nutrition.productId WHERE products.id = :id";
        $smt = $db->prepare($query);
        $smt->execute([
            'id' => $id
        ]);
        $result = $smt->fetch(PDO::FETCH_ASSOC);

        $product = new Product(
            intval($result['product_id']),
            intval($result['stock']),
            $result['name'],
            $result['description'],
            floatval($result['price']),
            new DateTime($result['createdAt']),
            new DateTime($result['updatedAt']),
            new Nutrition(
                intval($result['nutrition_id']),
                intval($result['product_id']),
                intval($result['energyKj']),
                intval($result['energyKcal']),
                floatval($result['fats']),
                floatval($result['saturatedFats']),
                floatval($result['carbohydrates']),
                floatval($result['sugars']),
                floatval($result['protein']),
                floatval($result['salt']),
                new DateTime($result['createdAt']),
                new DateTime($result['updatedAt']),
            )
        );
        return $product;
    }

    public static function search(string $search)
    {
        $db = self::connectDB();
        $query = "SELECT *, products.id as product_id, nutrition.id as nutrition_id FROM products 
            JOIN nutrition ON products.id = nutrition.productId
            WHERE name LIKE :search";
        $smt = $db->prepare($query);
        $smt->execute(['search' => $search]);
        $results = $smt->fetchAll(PDO::FETCH_ASSOC);
        $productList = [];
        foreach ($results as $result) {
            $product = new Product(
                intval($result['product_id']),
                intval($result['stock']),
                $result['name'],
                $result['description'],
                floatval($result['price']),
                new DateTime($result['createdAt']),
                new DateTime($result['updatedAt']),
                new Nutrition(
                    intval($result['nutrition_id']),
                    intval($result['product_id']),
                    intval($result['energyKj']),
                    intval($result['energyKcal']),
                    floatval($result['fats']),
                    floatval($result['saturatedFats']),
                    floatval($result['carbohydrates']),
                    floatval($result['sugars']),
                    floatval($result['protein']),
                    floatval($result['salt']),
                    new DateTime($result['createdAt']),
                    new DateTime($result['updatedAt']),
                )
            );
            $productList[] = $product;
        }
        return $productList;
    }


    public static function getProducts()
    {
        $db = self::connectDB();


        $query = "SELECT *, products.id as product_id, nutrition.id as nutrition_id FROM products JOIN nutrition on products.id = nutrition.productId";
        $smt = $db->prepare($query);
        $smt->execute();
        $results = $smt->fetchAll(PDO::FETCH_ASSOC);

        $productList = [];
        foreach ($results as $result) {
            $product = new Product(
                intval($result['product_id']),
                intval($result['stock']),
                $result['name'],
                $result['description'],
                floatval($result['price']),
                new DateTime($result['createdAt']),
                new DateTime($result['updatedAt']),
                new Nutrition(
                    intval($result['nutrition_id']),
                    intval($result['product_id']),
                    intval($result['energyKj']),
                    intval($result['energyKcal']),
                    floatval($result['fats']),
                    floatval($result['saturatedFats']),
                    floatval($result['carbohydrates']),
                    floatval($result['sugars']),
                    floatval($result['protein']),
                    floatval($result['salt']),
                    new DateTime($result['createdAt']),
                    new DateTime($result['updatedAt']),
                )
            );
            $productList[] = $product;
        }
        return $productList;
    }

    public static function uploadImage(string $tmpName, string $contentType, int $fileId)
    {
        $db = self::connectDB();
        $stmt = $db->prepare("UPDATE products SET image = :imageBlob, imageType = :imageType WHERE id = :id");

        $fp = fopen($tmpName, 'r');

        $stmt->bindParam(':imageBlob', $fp, PDO::PARAM_LOB);
        $stmt->bindParam(':imageType', $contentType);
        $stmt->bindParam(':id', $fileId);
        $db->beginTransaction();
        $stmt->execute();
        $db->commit();
    }

    public static function getImageBlob(int $fileId)
    {
        $db = self::connectDB();
        $stmt = $db->prepare('SELECT imageType, image FROM products WHERE id = :id');
        $stmt->execute(['id' => $fileId]);
        $stmt->bindColumn('imageType', $type, PDO::PARAM_STR, 256);
        $stmt->bindColumn('image', $lob, PDO::PARAM_LOB);
        $stmt->fetch(PDO::FETCH_BOUND);
        if ($type == null || $lob == null) {
            return null;
        } else {
            return [
                'contentType' => $type,
                'lob' => $lob
            ];
        }
    }

    public static function delete(int $productId)
    {
        $db = self::connectDB();
        $query = "DELETE FROM products WHERE id = :id";
        $stm = $db->prepare($query);
        $stm->execute(['id' => $productId]);
        if ($stm->rowCount()) {
            return true;
        } else return false;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $stock
     */
    public function setStock(int $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return Nutrition
     */
    public function getNutrition(): Nutrition
    {
        return $this->nutrition;
    }


}