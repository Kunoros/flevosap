<?php

class getProduct extends ConnectDB
{
    public static function showProducts()
    {
        $dbConn = self::connectDB();
        $stmt = $dbConn->prepare('SELECT name,description, price,stock,id FROM products');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public static function showSpesificProduct()
    {
        $url = $_SERVER['REQUEST_URI'];
        $id = substr($url, strrpos($url, '=') + 1);
        $dbConn = self::connectDB();
        $stmt = $dbConn->prepare('SELECT name,description,price,stock,productid,energyKj,energyKcal,fats,saturatedFats,carbohydrates,sugars,protein,salt
        FROM nutrition  
        JOIN products p on p.id = nutrition.productId
        WHERE productid = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public static function cartedProducts($id)
    {
        $dbConn = self::connectDB();
            $stmt = $dbConn->prepare('SELECT id,name,price,stock FROM products WHERE id = ?');
            $stmt->execute([$id]);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}