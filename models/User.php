<?php

class User extends ConnectDB
{

    private int $id;
    private string $firstName;
    private string $lastName;
    private string $email;
    private string $street;
    private int $houseNumber;
    private string $postalCode;
    private string $city;
    private string $role;

    /**
     * User constructor.
     * @param int $id
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $street
     * @param int $houseNumber
     * @param string $postalCode
     * @param string $city
     * @param string $role
     */
    public function __construct(int $id, string $firstName, string $lastName, string $email, string $street, int $houseNumber, string $postalCode, string $city, string $role)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->street = $street;
        $this->houseNumber = $houseNumber;
        $this->postalCode = $postalCode;
        $this->city = $city;
        $this->role = $role;
    }


    public static function checkUser($email)
    {
        $dbConn = self::connectDB();
        $stm = $dbConn->prepare("SELECT email FROM users WHERE email = :email");

        $stm->bindValue(':email', $email);
        $stm->execute();

        $emailExist = $stm->fetch();
        if ($emailExist) {
            return false;
        } else {
            return true;
        }
    }

    public static function createUser($data)
    {

        $dbConn = self::connectDB();
        $password = password_hash($data['password'], PASSWORD_BCRYPT);

        $stm = $dbConn->prepare("INSERT INTO users (firstName, lastName, email, passwordHash, street, houseNumber, postalCode, city) VALUES (:firstName, :lastName, :email, :passwordHash, :street, :houseNumber, :postalCode, :city)");

        $stm->bindValue(':firstName', $data['firstName']);
        $stm->bindValue(':lastName', $data['lastName']);
        $stm->bindValue(':email', $data['email']);
        $stm->bindValue(':passwordHash', $password);
        $stm->bindValue(':street', $data['street']);
        $stm->bindValue(':houseNumber', $data['houseNumber']);
        $stm->bindValue(':postalCode', $data['postcode']);
        $stm->bindValue(':city', $data['city']);

        if (User::checkUser($data['email']) === false) {
            header('Location: ./register?register=email already in use');
        } else {
            $stm->execute();
            header('Location: ./login');
        }
    }

    public static function userLogin($data)
    {
        $userEmail = $data['email'];
        $userPassword = $data['password'];

        $dbConn = self::connectDB();
        $stm = $dbConn->prepare("SELECT * FROM users WHERE email = :email");
        $stm->bindParam(':email', $userEmail);

        if ($stm->execute() && $stm->rowCount() > 0) {
            $user = $stm->fetch(PDO::FETCH_ASSOC);

            if (password_verify($userPassword, $user['passwordHash'])) {
                return new User(
                    $user['id'],
                    $user['firstName'],
                    $user['lastName'],
                    $user['email'],
                    $user['street'],
                    $user['houseNumber'],
                    $user['postalCode'],
                    $user['city'],
                    $user['role']
                );
            } else {
                header('Location: ./login?login=wrong password');
            }
        } else {
            header('Location: ./login?login=email not found');
        }
    }

    public static function updateUser($data)
    {
        $user = User::getUser($data['id']);



        if ($user) {
            $passwordChange = false;
            if ($data['old_password'] !== '' && $data['new_password'] !== '' && $data['new_password2'] !== '') {
                if (User::verifyUserPassword($data['id'], $data['old_password']) && $data['new_password'] == $data['new_password2']) {
                    $passwordChange = true;
                }
            }
            $dbConn = self::connectDb();
            if ($data['email'] == $user->email) {
                $stm = $dbConn->prepare("UPDATE users SET firstName = :firstName, lastName = :lastName, street = :street, houseNumber = :houseNumber, postalCode = :postalCode, city = :city".($passwordChange ? ', passwordHash = :password':'')." WHERE id= :id");
            } else {
                $stm = $dbConn->prepare("UPDATE users SET email = :email, firstName = :firstName, lastName = :lastName, street = :street, houseNumber = :houseNumber, postalCode = :postalCode, city = :city".($passwordChange ? ', passwordHash = :password':'')." WHERE id= :id");
                $stm->bindValue(':email', $data['email']);
            }

            $stm->bindValue(':firstName', $data['firstName']);
            $stm->bindValue(':lastName', $data['lastName']);
            $stm->bindValue(':street', $data['street']);
            $stm->bindValue(':houseNumber', $data['houseNumber']);
            $stm->bindValue(':postalCode', $data['postalCode']);
            $stm->bindValue(':city', $data['city']);
            $stm->bindValue(':id', $user->id);

            if ($passwordChange) $stm->bindValue(':password', password_hash($data['new_password'], PASSWORD_BCRYPT));

            if ($stm->execute()) {
                return User::getUser($user->id);
            } else {
                header('Location: ./profile?edit=Something went wrong try again');
                exit;
            }
        } else {
            header('Location: ./profile?edit=User not found');
            exit;
        }
    }

    public static function getUser($id)
    {
        $dbConn = self::connectDB();
        $stm = $dbConn->prepare("select * from users where id = :id");
        $stm->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stm->execute() && $stm->rowCount() > 0) {
            $user = $stm->fetch();
            return new User(
                $user['id'],
                $user['firstName'],
                $user['lastName'],
                $user['email'],
                $user['street'],
                $user['houseNumber'],
                $user['postalCode'],
                $user['city'],
                $user['role']
            );
        }
        return null;
    }

    public static function verifyUserPassword($id, $password)
    {
        $dbConn = self::connectDb();
        $stm = $dbConn->prepare("SELECT * FROM users WHERE id = :id");
        $stm->bindParam(':id', $id);
        if ($stm->execute() && $stm->rowCount() > 0) {
            $user = $stm->fetch();
            if (password_verify($password, $user['passwordHash'])) {
                return true;
            } else {
                header('Location: ./profile?edit=Verify went wrong');
                exit;
            }
        }
    }

    public static function getAll()
    {
        $dbConn = self::connectDB();
        $stm = $dbConn->prepare("SELECT * FROM users");
        $stm->execute();
        $users = $stm->fetchAll(PDO::FETCH_ASSOC);
        $userList = [];
        foreach ($users as $user) {
            $newUser = new User(
                $user['id'],
                $user['firstName'],
                $user['lastName'],
                $user['email'],
                $user['street'],
                $user['houseNumber'],
                $user['postalCode'],
                $user['city'],
                $user['role']
            );
            $userList[] = $newUser;
        }
        return $userList;
    }

    /**
     * Returns formatted string of adress
     * @return string
     */
    public function getAddress(): string
    {
        return sprintf('%s %d %s', $this->street, $this->houseNumber, $this->postalCode );
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @return int
     */
    public function getHouseNumber(): int
    {
        return $this->houseNumber;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }
}
