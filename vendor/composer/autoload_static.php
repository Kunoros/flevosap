<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitfcb26ba9258ae042e3679042e7b38b45
{
    public static $classMap = array (
        'BaseController' => __DIR__ . '/../..' . '/controllers/BaseController.php',
        'ComposerAutoloaderInitfcb26ba9258ae042e3679042e7b38b45' => __DIR__ . '/..' . '/composer/autoload_real.php',
        'Composer\\Autoload\\ClassLoader' => __DIR__ . '/..' . '/composer/ClassLoader.php',
        'Composer\\Autoload\\ComposerStaticInitfcb26ba9258ae042e3679042e7b38b45' => __DIR__ . '/..' . '/composer/autoload_static.php',
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'ConnectDB' => __DIR__ . '/../..' . '/models/ConnectDB.php',
        'Nutrition' => __DIR__ . '/../..' . '/models/Nutrition.php',
        'Order' => __DIR__ . '/../..' . '/models/Order.php',
        'Product' => __DIR__ . '/../..' . '/models/Product.php',
        'Router' => __DIR__ . '/../..' . '/core/Router.php',
        'User' => __DIR__ . '/../..' . '/models/User.php',
        'View' => __DIR__ . '/../..' . '/controllers/View.interface.php',
        'adminProducts' => __DIR__ . '/../..' . '/controllers/AdminProducts.php',
        'betalen' => __DIR__ . '/../..' . '/controllers/Betalen.php',
        'boomgaard' => __DIR__ . '/../..' . '/controllers/boomgaard.php',
        'contact' => __DIR__ . '/../..' . '/controllers/contact.php',
        'getProduct' => __DIR__ . '/../..' . '/models/getProduct.php',
        'home' => __DIR__ . '/../..' . '/controllers/home.php',
        'horeca' => __DIR__ . '/../..' . '/controllers/horeca.php',
        'login' => __DIR__ . '/../..' . '/controllers/login.php',
        'nieuws' => __DIR__ . '/../..' . '/controllers/nieuws.php',
        'order' => __DIR__ . '/../..' . '/controllers/order.php',
        'orderlist' => __DIR__ . '/../..' . '/controllers/orderlist.php',
        'products' => __DIR__ . '/../..' . '/controllers/products.php',
        'profile' => __DIR__ . '/../..' . '/controllers/profile.php',
        'register' => __DIR__ . '/../..' . '/controllers/register.php',
        'shoppingCart_controller' => __DIR__ . '/../..' . '/controllers/shoppingCart_controller.php',
        'webshop' => __DIR__ . '/../..' . '/controllers/webshop.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInitfcb26ba9258ae042e3679042e7b38b45::$classMap;

        }, null, ClassLoader::class);
    }
}
