<?php

$routes = [
    'GET' => [
        '' => [
            'controller' => 'home',
            'method' => 'getHomeView'
        ],
        'home' => [
            'controller' => 'home',
            'method' => 'getHomeView'
        ],
        'boomgaard' => [
            'controller' => 'boomgaard',
            'method' => 'getBoomgaardView'
        ],
        'webshop' => [
            'controller' => 'webshop',
            'method' => 'getWebshopView'
        ],
        'horeca' => [
            'controller' => 'horeca',
            'method' => 'getHorecaView'
        ],
        'nieuws' => [
            'controller' => 'nieuws',
            'method' => 'getNieuwsView'
        ],
        'contact' => [
            'controller' => 'contact',
            'method' => 'getContactView'
        ],
        'login' => [
            'controller' => 'login',
            'method' => 'getLoginView'
        ],
        'register' => [
            'controller' => 'register',
            'method' => 'getRegisterView'
        ],
        'logout' => [
            'controller' => 'login',
            'method' => 'logoutUser'
        ],
        'winkelwagen' => [
            'controller' => 'shoppingCart_controller',
            'method' => 'getShoppingCartView',
            'roles' => ["ADMIN", "EMPLOYEE","CUSTOMER"]
        ],
        'admin-product' => [
            'controller' => 'adminProducts',
            'method' => 'loadView'
        ],
        'admin-users' => [
            'controller' => 'AdminUserOverview',
            'method' => 'loadView'
        ],
        'profile' => [
            'controller' => 'profile',
            'method' => 'getProfileView'
        ],
        'products' => [
            'controller' => 'products',
            'method' => 'getProductView'
        ],
        'orderlist' => [
            'controller' => 'orderlist',
            'method' => 'getorderlistview',
            'roles' => ["ADMIN", "EMPLOYEE"]
        ],
        'product/image' => [
            'controller' => 'products',
            'method' => 'getImage'
        ]

    ],
    'POST' => [
        'winkelwagen' => [
            'controller' => 'shoppingCart_controller',
            'method' => 'shoppingCart'
        ],
        'winkelwagen-remove' => [
            'controller' => 'shoppingCart_controller',
            'method' => 'removeShoppingCartItem'
        ],
        'winkelwagen-update' => [
            'controller' => 'shoppingCart_controller',
            'method' => 'updateShoppingCart'
        ],
        'register-post' => [
            'controller' => 'register',
            'method' => 'register'
        ],
        'login-post' => [
            'controller' => 'login',
            'method' => 'loginUser'
        ],
        'remove-product' => [
            'controller' => 'adminProducts',
            'method' => 'removeProduct'
        ],
        'update-product' => [
            'controller' => 'adminProducts',
            'method' => 'updateProduct'
        ],
        'search' => [
            'controller' => 'products',
            'method' => 'getSearch'
        ],
        'update-profile' => [
            'controller' => 'profile',
            'method' => 'profileUpdate'
        ],
        'product/image' => [
            'controller' => 'AdminProducts',
            'method' => 'updateImage'
        ],
        'bestel' => [
            'controller' => 'order',
            'method' => 'getOrderView'
        ],
        'betalen' => [
            'controller' => 'betalen',
            'method' => 'getBetalenView'
        ],
        'order' => [
            'controller' => 'betalen',
            'method' => 'order'
        ],
        'home' => [
            'controller' => 'home',
            'method' => 'getHomeView'
        ],
        'contact' => [
            'controller' => 'contact',
            'method' => 'sendContactEmail'
        ],
    ],
];